var lc = document.getElementById('lc-text');
var button = document.getElementById('lc-button');
var recognizing = false;
var recognition = new webkitSpeechRecognition();
// var sessions = loadFromLocalStorage('sessions');
var questions = loadFromLocalStorage('questions')
var currentQuestion = null;
var transcript = [];
var startTime;
var beginTime;
var autoRestart = false;
recognition.continuous = true;
recognition.interimResults = true;

recognition.onstart = function() {
  recognizing = true;
  recordSession();
};

recognition.onerror = function(event) {
  console.log("Recognition error: ", event.message ? event.message : event.error);
  if ((event.error == "no-speech") || (event.error == "audio-capture") || (event.error == "network") || (event.error == "bad-grammar")){
    refresh();
  }
};

recognition.onend = function() {
  recognizing = false;
  if (autoRestart){
    toggleSpeechRecognition();
  }
};

recognition.onresult = function(event) {
  for (var i = event.resultIndex; i < event.results.length; ++i) {
    if(event.results[i][0].confidence > 0.4) {
      lc.firstChild.data = capitalize(event.results[i][0].transcript);
      if (event.results[i].isFinal) {
        saveResponse(questions[currentQuestion], event.results[i][0].transcript);
      }
    }
  }
};


function capitalize(s) {
  var first_char = /\S/;
  return s.replace(first_char, function(m) {
    return m.toUpperCase();
  });
}

function toggleSpeechRecognition(event) {
  if(recognizing) {
    recognition.stop();
    lc.style.display = "none";
    lc.firstChild.data = "Begin speaking. Click to stop.";
    button.style.display = "inline-block";    
    $('body').removeClass('lc-on');
    showRecordings();
    return;
  } else {
    lc.style.display = "inline-block";
    button.style.display = "none";
    $('body').addClass('lc-on');
    recognition.start();    
  }
  autoRestart = false;
}

function refresh(event) {
  autoRestart = true;
  recognizing = false;
  recognition.abort();
  try {
    recognition.start()
  }catch(e){
  }
}

function newSession(event) {
  window.location.hash = '';
  currentQuestion = null;
  toggleSpeechRecognition(event);
}

function genQs() {
  if (! questions){
  questions = ['Tell me about yourself', 'What is one challenge that you experienced and how did you solve it?', 'What is your favorite job and why?'];
  saveToLocalStorage('questions', questions);
}
 else {
  showRecordings();  
  }
}

function locationHashChanged() {
  recognition.stop();
  currentQuestion = null;
  if (window.location.hash){
    try {
      recognition.start();      
    }catch(e){
      recognizing = true;
      recordSession();
    }
  }
}


function showRecordings (){
  // Populate and show recordings section - maybe only if they're on a page with a hash tag
  
  var select = document.getElementById('session-select');
  
  // Clear any existing options, since we're refreshing after recording
  for(i = select.options.length - 1 ; i >= 0 ; i--){
    select.remove(i);
  }
  
  for(index in questions) {
      select.options[select.options.length] = new Option(questions[index], index);
  }
  if (window.location.hash){
    var selectValue = findSessionFromHash();
    if (selectValue){
      select.value = selectValue;
    }
  }
  select.style = "";
}

function continueSession(event){
  var select = document.getElementById('session-select');
  currentQuestion = select.value;
  toggleSpeechRecognition(event);
}

function viewSession() {
  var select = document.getElementById('session-select');
  var textArea = document.getElementById('transcript-textarea');
  var sessionData = loadFromLocalStorage(questions[select.value]);

  if (sessionData) {
    textArea.value = formatTranscriptText(sessionData);
    $('#lc-transcript').show();
  } else {
    textArea.value = '';
    $('#lc-transcript').hide();
  }
}


function findSessionFromHash() {
  // Look for the session
    var foundSession = null;
    var i = 0;
    var hashText = window.location.hash.replace("#", "");
    do {
      if (questions[i] === hashText){
        foundSession = i;
      }
      i++;
    } while ((foundSession === null) && (i < questions.length));
    return foundSession;
}

function recordSession(){
  if ((! currentQuestion) && (window.location.hash)){
    // Look for the session
    currentQuestion = findSessionFromHash();
  }
  transcript = [];

  // Set the begin time for the session, which may be a continuation
  beginTime = new Date();

  // Add hash to URL, so that a refresh will add to the same session
  window.location.hash = questions[currentQuestion];
}


function saveResponse(sessionName, text){
  if (text){
    var endTime = new Date();
    transcript.push({"startTime": beginTime, "endTime": endTime, "text": text});
    // Reset the beginTime
    beginTime = endTime;
    saveToLocalStorage(sessionName, JSON.stringify(transcript));
  }
}

function saveTranscript() {
  var select = document.getElementById('session-select');
  var textArea = document.querySelector('#lc-transcript textarea');
  var editedTranscript = textArea.value;
  var transcriptData = JSON.parse(loadFromLocalStorage(questions[select.value]));

  transcriptData = editedTranscript.split('\n').map(function(text, index) {
    var response = transcriptData[index];
    response.text = text.trim();
    return response;
  });

  saveToLocalStorage(questions[select.value], JSON.stringify(transcriptData));

  document.getElementById('lc-transcript').style.display = 'none';
}


function formatTranscriptText(transcript){
  if (typeof transcript === 'string') {
    return transcript;
  }

  var output = "";
  for (var i = 0; i < transcript.length; ++i) {
    output += transcript[i].text + "\n";
  }
  return output;
}

  function loadFromLocalStorage(key) {
    var savedJSON;

    if (localStorage) {
      try {
        savedJSON = JSON.parse(localStorage.getItem(key));
      } catch(e) {}
    }

    if (typeof savedJSON !== 'undefined') {
      return savedJSON;
    }
  }

function saveToLocalStorage(key, data) {
  if (data && localStorage) {
    if (typeof data === "object"){
      localStorage.setItem(key, JSON.stringify(data));
    }else{
      localStorage.setItem(key, data);
    }
  }
}

function getQueryParam(name) {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(name);
}
